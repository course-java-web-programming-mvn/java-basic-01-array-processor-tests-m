package com.epam.java_basic;

import com.epam.java_basic.array_processor.ArrayProcessor;
import com.epam.java_basic.utils.UserInterface;

import java.util.Random;

/**
 * Application's entry point, use it to demonstrate your code execution
 */
public class Application {

    private static final int MIN_ARRAY_VALUE = -10;
    private static final int MAX_ARRAY_VALUE = 10;
    private static final int ARRAY_SIZE = 20;
    private static final Random RANDOMIZER = new Random();

    public static void main(String[] args) {
        new Application().start();
    }

    private void start() {
        int[] array = generateArray(MIN_ARRAY_VALUE, MAX_ARRAY_VALUE, ARRAY_SIZE);
        ArrayProcessor processor = new ArrayProcessor();
        UserInterface.print("Original array:");
        UserInterface.printArray(array);
        UserInterface.print("1) Exchange max negative and min positive elements:");
        UserInterface.printArray(processor.swapMaxNegativeAndMinPositiveElements(array));
        UserInterface.print("2) Sum of elements on even positions:");
        UserInterface.print(processor.countSumOfElementsOnEvenPositions(array));
        UserInterface.print("3) Replace negative values with 0");
        UserInterface.printArray(processor.replaceEachNegativeElementsWithZero(array));
        UserInterface.print("4) Multiply by 3 each positive element standing before negative one");
        UserInterface.printArray(processor.multiplyByThreeEachPositiveElementStandingBeforeNegative(array));
        UserInterface.print("5) Difference between average and min element in array:");
        UserInterface.print(processor.calculateDifferenceBetweenAverageAndMinElement(array));
        UserInterface.print("6) Elements which present more than one time and stay on odd index");
        UserInterface.printArray(processor.findSameElementsStandingOnOddPositions(array));
    }

    private int[] generateArray(int min, int max, int size) {
        int[] result = new int[size];
        for (int i = 0; i < result.length; i++) {
            result[i] = RANDOMIZER.nextInt((max - min) + 1) + min;
        }
        return result;
    }

}
